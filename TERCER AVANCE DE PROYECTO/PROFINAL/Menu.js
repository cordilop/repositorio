import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { View, Text, StyleSheet, TextInput, ScrollView, Image, TouchableOpacity } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { useNavigation } from '@react-navigation/native';
import DatePicker from '@react-native-community/datetimepicker';
import { useState } from 'react';

const Tab = createBottomTabNavigator();
const Logoo = require("./assets/Images/logo.png");


export function MenuTabs() {
  return (
    <Tab.Navigator>
      <Tab.Screen name="Sobre Nosotros" component={NosotroScreen} options={{ headerShown: false }} />
      <Tab.Screen name="Citas" component={CitasScreen} options={{ headerShown: false }} />
      <Tab.Screen name="Maquinas" component={MaquinaScreen} options={{ headerShown: false }} />
    </Tab.Navigator>
  );
}

function CitasScreen() {
  const navigation = useNavigation();
  const [date, setDate] = useState(new Date());

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setDate(currentDate);
  };

  return (
    <View style={styles.containerCita}>
      <Text style={styles.title}>Agendar Cita</Text>
      <Image
        source={Logoo}
      />
      <Text style={styles.subtitle}>Fecha de la cita </Text>
      <DatePicker
        value={date}
        mode="date"
        is24Hour={true}
        display="default"
        onChange={onChange}
      />
      <Text style={styles.subtitle}>Hora de la cita </Text>
      <DatePicker
        value={date}
        mode="time"
        is24Hour={true}
        display="default"
        onChange={onChange}
      />

      <TouchableOpacity onPress={() => navigation.navigate('Menu')} style={styles.menuButton}>
        <LinearGradient
          colors={['#04b9f0', '#50d1f8', '#50d1f8']}
          start={{x: 1, y: 0}}
          end={{x: 0, y:1}}
          style={styles.linearGradient}>
          <Text style={styles.menuButtonText}>Hacer Cita!</Text>
        </LinearGradient>
      </TouchableOpacity>

    </View>
  );
}

export default CitasScreen;



function MaquinaScreen() {
  return (
    <View style={styles.container}>
      <Text style={styles.title} >Maquinas Screen</Text>
    </View>
  );
}

function NosotroScreen() {
  return (
    <View style={styles.container}>
      <Text style={styles.title} >Nosotros Screen</Text>
    </View>
  );
}

export function SignupForm() {
  return(
    <Tab.Navigator>
      <Tab.Screen name="Crear cuenta" component={SignupScreen} />
    </Tab.Navigator>
  );
}

function SignupScreen() {
  const navigation = useNavigation();
  return (
    <ScrollView contentContainerStyle={styles.scrollContainer} style={styles.container}>
      <Text style={styles.TextSignup}>Nombre Moral</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup}>Número telefonico</Text>
      <TextInput 
        style={styles.TextInput}
      />
       <Text style={styles.TextSignup} >Nombre de pila</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup} >Primer apellido</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup} >Segundo apellido</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup} >Colonia</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup} >Calle</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup} >Código Postal</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup} >Nombre de usuario</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup} >Contraseña</Text>
      <TextInput 
        style={styles.TextInput}
      />
      <Text style={styles.TextSignup} >Confirmar contraseña</Text>
      <TextInput 
        style={styles.TextInput}
      />

      <TouchableOpacity onPress={() => navigation.navigate('Menu')} style={styles.menuButton}>
        <LinearGradient
          colors={['#04b9f0', '#50d1f8', '#50d1f8']}
          start={{x: 1, y: 0}}
          end={{x: 0, y:1}}
          style={styles.linearGradient}>
          <Text style={styles.menuButtonText}>Crear Cuenta</Text>
        </LinearGradient>
      </TouchableOpacity>

      <TouchableOpacity onPress={() => navigation.navigate('Home')} style={styles.menuButton}>
        <LinearGradient
          colors={['#fff', '#fff', '#fff']}
          start={{x: 1, y: 0}}
          end={{x: 0, y:1}}
          style={styles.linearGradient}>
          <Text style={styles.cancelar} >Cancelar</Text>
        </LinearGradient>
      </TouchableOpacity>

    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  containerCita: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    
  },
  scrollContainer: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  TextInput: {
    padding: 10,
    paddingStart: 30,
    width: '60%',
    height: 35,
    marginTop: 20,
    borderRadius: 10,
    backgroundColor: '#f1f1f1',
    borderColor: '#000', 
    borderWidth: 0.3, 
  },
  TextSignup: {
    fontWeight: 'bold',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 30,
  },
  menuButton: {
    marginTop: 20,
    borderRadius: 10,
  },
  linearGradient: {
    marginTop: 20,
    width: 170,
    height: 50,
    borderRadius: 25,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuButtonText: {
    fontSize: 20,
    color: 'white',
    textAlign: 'center',
  },
  cancelar: {
    fontWeight: 'bold',
    paddingTop: 1,
    paddingBottom: 42,
  },
  title: {
    fontSize: 34,
    fontWeight: 'bold',
    marginBottom: 60,
    marginTop: 0,
  },
  subtitle: {
    fontSize: 23,
    marginTop: 50,
    marginBottom: 40,
  },
});
