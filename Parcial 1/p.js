const axios = require('axios');
const readline = require('readline');

const apiUrl = "http://jsonplaceholder.typicode.com/todos";

async function obtenerListaDePendientes(opcion) {
    try {
        const respuesta = await axios.get(apiUrl);
        const datos = respuesta.data;

        let listaFiltrada = [];
        switch (opcion) {
            case '1':
                listaFiltrada = datos.map((pendiente) => `ID: ${pendiente.id}`);
                break;
            case '2':
                listaFiltrada = datos.map((pendiente) => `ID: ${pendiente.id}, Title: ${pendiente.title}`);
                break;
            case '3':
                listaFiltrada = datos.filter((pendiente) => !pendiente.completed)
                    .map((pendiente) => `ID: ${pendiente.id}, Title: ${pendiente.title}`);
                break;
            case '4':
                listaFiltrada = datos.filter((pendiente) => pendiente.completed)
                    .map((pendiente) => `ID: ${pendiente.id}, Title: ${pendiente.title}`);
                break;
            case '5':
                listaFiltrada = datos.map((pendiente) => `ID: ${pendiente.id}, UserId: ${pendiente.userId}`);
                break;
            case '6':
                listaFiltrada = datos.filter((pendiente) => pendiente.completed)
                    .map((pendiente) => `ID: ${pendiente.id}, UserId: ${pendiente.userId}`);
                break;
            case '7':
                listaFiltrada = datos.filter((pendiente) => !pendiente.completed)
                    .map((pendiente) => `ID: ${pendiente.id}, UserId: ${pendiente.userId}`);
                break;
            default:
                console.log("Opción no válida.");
                break;
        }

        console.log(`Lista de Pendientes: \n${listaFiltrada.join('\n')}`);
        esperarEnterParaMostrarMenu();

    } catch (error) {
        console.error("Error al obtener la lista de pendientes:", error);
    }
}

function esperarEnterParaMostrarMenu() {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    rl.question('PRESIONA ENTER PARA VOLVER A MOSTRAR EL MENU', () => {
        rl.close();
        mostrarMenu();
    });
}

function mostrarMenu() {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });

    const opcionesValidas = ['1', '2', '3', '4', '5', '6', '7', 's'];

    rl.question(`¿Qué deseas mostrar? 
    1. Lista de todos los pendientes (solo IDs)
    2. Lista de todos los pendientes (IDs y Titles)
    3. Lista de todos los pendientes sin resolver (ID y Title)
    4. Lista de todos los pendientes resueltos (ID y Title)
    5. Lista de todos los pendientes (ID y UserId)
    6. Lista de todos los pendientes resueltos (ID y UserId)
    7. Lista de todos los pendientes sin resolver (ID y UserId)
    Seleccione uno de los numeros o "s" para salir: `, (opcionSeleccionada) => {
        rl.close();

        opcionSeleccionada = opcionSeleccionada.trim().toLowerCase();

        if (!opcionesValidas.includes(opcionSeleccionada)) {
            console.log("Opción no válida. Por favor, selecciona una opción válida.");
            mostrarMenu();
            return;
        }

        if (opcionSeleccionada === 's') {
            console.log("Saliendo del programa.");
            process.exit();
        }

        obtenerListaDePendientes(opcionSeleccionada);
    });
}

mostrarMenu();
