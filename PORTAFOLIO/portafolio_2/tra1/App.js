import React, { useState } from 'react';
import { View, Image, Button, StyleSheet, TouchableOpacity, Modal, Text } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import { Ionicons } from '@expo/vector-icons';

const EmojiPicker = ({ onSelectEmoji, onClose }) => {
  const emojis = ['😊', '🌟', '❤️', '😂', '😍', '🎉'];

  return (
    <Modal visible={true} transparent={true} animationType="slide">
      <View style={styles.modalContainer}>
        <View style={styles.modalContent}>
          {emojis.map((emoji, index) => (
            <TouchableOpacity key={index} onPress={() => onSelectEmoji(emoji)} style={styles.emojiButton}>
              <Text style={styles.emoji}>{emoji}</Text>
            </TouchableOpacity>
          ))}
          <Button title="Cerrar" onPress={onClose} />
        </View>
      </View>
    </Modal>
  );
};

export default function App() {
  const [selectedImage, setSelectedImage] = useState(null);
  const [isEmojiPickerVisible, setIsEmojiPickerVisible] = useState(false);
  const [selectedEmoji, setSelectedEmoji] = useState(null);

  const pickImage = async () => {
    let permissionResult = await ImagePicker.requestMediaLibraryPermissionsAsync();

    if (permissionResult.granted === false) {
      alert("Permission to access camera roll is required!");
      return;
    }

    let pickerResult = await ImagePicker.launchImageLibraryAsync();
    
    if (pickerResult.cancelled === true) {
      return;
    }

    setSelectedImage({ localUri: pickerResult.uri });
  };

  const toggleEmojiPicker = () => {
    setIsEmojiPickerVisible(!isEmojiPickerVisible);
  };

  const selectEmoji = (emoji) => {
    setSelectedEmoji(emoji);
    toggleEmojiPicker();
  };

  return (
    <View style={styles.container}>
      <View style={styles.buttonsContainer}>
        <Button title="Seleccionar imagen" onPress={pickImage} />
        <TouchableOpacity onPress={toggleEmojiPicker} style={styles.emojiButton}>
          <Text style={styles.emojiButtonText}>Seleccionar Emoji</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.imageContainer}>
        {selectedImage ? (
          <Image source={{ uri: selectedImage.localUri }} style={styles.image} />
        ) : (
          <Image source={require('./components/1.jpg')} style={styles.defaultImage} />
        )}
      </View>
      <Modal
        visible={isEmojiPickerVisible}
        transparent={true}
        animationType="slide"
        onRequestClose={toggleEmojiPicker}>
        <EmojiPicker onSelectEmoji={selectEmoji} onClose={toggleEmojiPicker} />
      </Modal>
      {selectedEmoji && (
        <Text style={styles.selectedEmoji}>{selectedEmoji}</Text>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ADD8E6',
  },
  buttonsContainer: {
    flexDirection: 'row',
    marginBottom: 20,
  },
  emojiButton: {
    marginLeft: 10,
    padding: 10,
    backgroundColor: '#6495ED',
    borderRadius: 5,
  },
  emojiButtonText: {
    color: 'white',
    fontSize: 16,
  },
  imageContainer: {
    alignItems: 'center',
    marginBottom: 20,
  },
  image: {
    width: 300,
    height: 300,
    resizeMode: 'contain',
  },
  defaultImage: {
    width: 200,
    height: 200,
    resizeMode: 'contain',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    backgroundColor: '#fff',
    padding: 20,
    borderRadius: 10,
  },
  emoji: {
    fontSize: 30,
  },
  selectedEmoji: {
    fontSize: 40,
    marginTop: 20,
  },
});
