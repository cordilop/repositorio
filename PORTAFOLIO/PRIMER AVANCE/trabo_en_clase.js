//  Función recursiva que saluda un número de veces.
var say = function say(veces) {
    say = undefined; // Desactiva la función después de la primera llamada.
    if (veces > 0) {
        console.log('¡Hola!');
        say(veces - 1);
    }
}
var sayHelloTimes = say; // Copia la función a otra variable.
say = "oops"; // Cambia la función original.
sayHelloTimes(2); // Invoca la función original dos veces.

///////////////////////////////////////////////////////////////////////

//  Función que registra mensajes para una persona.
function personLogsSomeThings(persona, ...msg) {
    msg.forEach(arg => {
        console.log(persona, 'dice', arg);
    });
}
personLogsSomeThings('John', 'hola', 'mundo'); // Llama a la función con mensajes.

///////////////////////////////////////////////////////////////////////

// Sección 3: Obtención de datos de la API de Stack Exchange y registro de títulos de preguntas.
var url = "https://api.stackexchange.com/2.2/questions/featured?order=desc&sort=activity&site=stackoverflow";
const responseData = fetch(url).then(response => response.json());
responseData.then(({ items }) => {
    for (const { title } of items) {
        console.log("Título de la pregunta: " + title + "?");
    }
})

function foo() {
    var a = 'hello';
    function bar() {
    var b = 'world';
    console.log(a); // => 'hello'
    console.log(b); // => 'world'
    }
    console.log(a); // => 'hello'
    console.log(b); // reference error
    }
    console.log(a); // reference error
    console.log(b); // reference erro

    //////////////////////////////////////////

    function foo() {
        const a = true;
        function bar() {
        const a = false; // different variable
        console.log(a); // false
        }
        const a = false; // SyntaxError
        a = false; // TypeError
        console.log(a); // true
        }

////////////////////////////////////////////////////

var prism = function(l, w, h) {
    return l * w * h;
    }

    
    function prism(l) {
        return function(w) {
        return function(h) {
        return l * w * h;
        }
        }
        }
/////////////////////////////////////

var namedSum = function sum (a, b) { // named
    return a + b;
    }
    var anonSum = function (a, b) { // anonymous
    return a + b;
    }
    namedSum(1, 3);
    anonSum(1, 3);

///////////////////////////////////////////////////////
var say = function say (times) {
    if (times > 0) {
    console.log('Hello!');

    // it uses the named function
    say(times - 1);
    }
    }
    var sayHelloTimes = say;
    say = "oops";
    sayHelloTimes(2)
    ///////////////////////////////