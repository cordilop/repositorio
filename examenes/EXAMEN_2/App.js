import React, { useState, useEffect } from 'react';
import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, FlatList, TouchableOpacity } from 'react-native';

export default function App() {
  const [todos, setTodos] = useState([]);
  const [displayedTodos, setDisplayedTodos] = useState([]);
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState(null);
  const menuOptions = [
    "Lista de todos los pendientes (solo IDs)",
    "Lista de todos los pendientes (IDs y Titles)",
    "Lista de todos los pendientes sin resolver (ID y Title)",
    "Lista de todos los pendientes resueltos (ID y Title)",
    "Lista de todos los pendientes (ID y UserId)",
    "Lista de todos los pendientes resueltos (ID y UserId)",
    "Lista de todos los pendientes sin resolver (ID y UserId)"
  ];

  useEffect(() => {
    fetch('http://jsonplaceholder.typicode.com/todos')
      .then(response => response.json())
      .then(data => {
        setTodos(data);
        setDisplayedTodos(data);
      })
      .catch(error => console.error('Error fetching todos:', error));
  }, []);

  const handleMenuSelection = (index) => {
    setSelectedOption(index);
    setIsMenuOpen(false);
    switch(index) {
      case 0:
        setDisplayedTodos(todos.map(todo => ({ id: todo.id })));
        break;
      case 1:
        setDisplayedTodos(todos.map(todo => ({ id: todo.id, title: todo.title })));
        break;
      case 2:
        setDisplayedTodos(todos.filter(todo => !todo.completed).map(todo => ({ id: todo.id, title: todo.title })));
        break;
      case 3:
        setDisplayedTodos(todos.filter(todo => todo.completed).map(todo => ({ id: todo.id, title: todo.title })));
        break;
      case 4:
        setDisplayedTodos(todos.map(todo => ({ id: todo.id, userId: todo.userId })));
        break;
      case 5:
        setDisplayedTodos(todos.filter(todo => todo.completed).map(todo => ({ id: todo.id, userId: todo.userId })));
        break;
      case 6:
        setDisplayedTodos(todos.filter(todo => !todo.completed).map(todo => ({ id: todo.id, userId: todo.userId })));
        break;
      default:
        setDisplayedTodos([]);
        break;
    }
  };

  const toggleMenu = () => {
    setIsMenuOpen(!isMenuOpen);
  };

  const renderItem = ({ item }) => (
    <View style={styles.todoItem}>
      <Text style={styles.todoText}>ID: {item.id}</Text>
      {item.title && <Text style={styles.todoText}>Title: {item.title}</Text>}
      {item.userId && <Text style={styles.todoText}>User ID: {item.userId}</Text>}
    </View>
  );

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.menuToggle} onPress={toggleMenu}>
        <Text style={styles.menuToggleText}>☰ Menu</Text>
      </TouchableOpacity>
      {isMenuOpen && (
        <View style={styles.menu}>
          {menuOptions.map((option, index) => (
            <TouchableOpacity key={index} style={styles.menuItem} onPress={() => handleMenuSelection(index)}>
              <Text style={[styles.menuText, selectedOption === index && styles.selectedMenuItem]}>{option}</Text>
            </TouchableOpacity>
          ))}
        </View>
      )}
      <FlatList
        data={displayedTodos}
        renderItem={renderItem}
        keyExtractor={(item, index) => index.toString()}
        style={styles.flatList}
      />
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  menuToggle: {
    position: 'absolute',
    top: 10,
    right: 10,
    zIndex: 1,
    backgroundColor: '#ff8c00',
    padding: 10,
    borderRadius: 5,
  },
  menuToggleText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#fff',
  },
  menu: {
    position: 'absolute',
    top: 50,
    right: 10,
    backgroundColor: '#fff',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ccc',
    zIndex: 1,
  },
  menuItem: {
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  menuText: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#333',
  },
  selectedMenuItem: {
    backgroundColor: '#ddd',
  },
  todoItem: {
    padding: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
  },
  todoText: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  flatList: {
    width: '100%',
  },
});
